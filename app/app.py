from flask import Flask, render_template, json, jsonify, request, redirect, url_for
import requests
import py_weather

app = Flask(__name__, static_url_path='/static')

@app.route('/weather', methods=['GET', 'POST'])
def weather_info():

    city = request.args.get('city')

    # checking if a GET request to the API is desired
    if request.method == 'GET':

        if 'refresh_button' in request.args:

            py_weather.get_weather(city, refresh=True)

        if 'edit_button' in request.args:

            temp = request.args.get('temp')
            description = request.args.get('description')
            wind = request.args.get('wind')
            py_weather.change_weather(city, temp, description, wind)

        weather_data = py_weather.get_weather(city)

        print(weather_data)

        try:

            city = weather_data['data']['name']
            temp = weather_data['data']['main']['temp']
            if not str(temp).endswith('F'):
                temp = str(temp) + u' \N{DEGREE SIGN}' + 'F'
            description = weather_data['data']['weather'][0]['description']
            wind = weather_data['data']['wind']['speed']
            if not str(wind).endswith('mph'):
                wind = str(wind) + ' mph'
            timestamp = weather_data['data']['timestamp']

            outfit_data = py_weather.get_outfit(city, temp, description)

            print(outfit_data)

            top_outfit = "static/images/" + outfit_data['top'] + ".png"
            bottom_outfit = "static/images/" + outfit_data['bottom'] + ".png"
            shoes_outfit = "static/images/" + outfit_data['shoes'] + ".png"

            try:
                food_data = py_weather.get_food(city)
                if (float(temp[:-2]) > 60):
                    food_type = "Warm? Have some ice cream!" 
                    food_name = food_data['warm_restaurants']['businesses'][0]['name']
                    food_address = ", ".join((str(i) for i in food_data['warm_restaurants']['businesses'][0]['location']['display_address']))
                    food_image = food_data['warm_restaurants']['businesses'][0]['image_url']
                else:
                    food_type = "Cold? Have some coffee!"
                    food_name = food_data['cold_restaurants']['businesses'][0]['name']
                    food_address = ", ".join((str(i) for i in food_data['cold_restaurants']['businesses'][0]['location']['display_address']))
                    food_image = food_data['cold_restaurants']['businesses'][0]['image_url']
                
                display_type = 'huh'
            except Exception as e:
                print(str(e))
                food_type = "No food found for this location!"
                food_name, food_address, food_image = '', '', None
                display_type = 'none'


            return render_template('weather.html', 
                city=city, temp=temp, description=description, wind=wind, timestamp=timestamp, top_outfit=top_outfit, 
                bottom_outfit=bottom_outfit, shoes_outfit=shoes_outfit, food_type=food_type, food_name=food_name, 
                food_address=food_address, food_image=food_image, display_type=display_type)

        except Exception as e:
            print(str(e))
            return redirect(url_for('error'))

    elif request.method == 'POST':
        # this will force a refresh
        city = request.form.get('city')
        temp = request.form.get('temp')
        description = request.form.get('description')
        wind = request.form.get('wind')

        py_weather.add_city(city, temp, description, wind)

        return render_template('index.html')

    else:
        return redirect(url_for('error'))

    return render_template('weather.html', city=city, temp=temp, description=description, wind=wind, timestamp=timestamp)


@app.route('/')
def weather_search():
    return render_template("index.html")

@app.route('/create')
def create_city():
    return render_template("create.html")


@app.route('/error')
def error():
    return render_template('error.html')


if __name__ == '__main__':
    app.run(debug=True, host="129.74.153.192", port=52098)
