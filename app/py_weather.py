from flask import json, jsonify
import requests

url = 'http://student04.cse.nd.edu:52097/weather/'
outfit_url = 'http://student04.cse.nd.edu:52097/outfit/'
food_url = 'http://student04.cse.nd.edu:52097/food/'

def get_weather(city, refresh=False):

	if refresh:
		return requests.get(url + city, headers={'refresh': 'force'})
	return requests.get(url + city).json()

def change_weather(city, temp, description, wind):

	return requests.put(url, data=json.dumps({'city': city, 'temp': temp, 'description': description, 'wind': wind})).json()

def add_city(city, temp, description, wind):

	return requests.post(url, data=json.dumps({'city': city, 'temp': temp, 'description': description, 'wind': wind})).json()

def get_outfit(city, temp, description):

	return requests.post(outfit_url, data=json.dumps({'curr_city': city, 'search_city': city, 'temp': temp, 'description': description})).json()

def get_food(city):
	return requests.get(food_url + city).json()