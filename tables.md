# Restful API Description

| Resource      | GET                                          | PUT                                      | POST                                            | DELETE |
|---------------|----------------------------------------------|------------------------------------------|-------------------------------------------------|--------|
| /weather/:key | Returns weather info for the city specified. |                                          |                                                 |        |
| /weather      |                                              | Updates weather info in a specified city | Creates a new city to be added to the database. |        |
| /outfit/      |                                              |                                          | Returns an outfit recommendation based on data. |        |
| /food/:key    | Returns food recommendations based on city.  |                                          |                                                 |        |

# JSON Guide

| Command | Resource      | Input Example                                                                                                                                                    | Output Example                                                              |
|---------|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| GET     | /weather/:key | none                                                                                                                                                             | {"result": "success", "data": "[cityinfo]"}                                 |
| POST    | /weather/     | {"timestamp": timestamp, "weather": [{"main": "Rain", "description": "light drizzle"}],"main":{"temp": "45"}, "wind":{"speed": "4"},"name": "Chicago","cod":200} | {"result": "success"}                                                       |
| PUT     | /weather/     | {"city": "Chicago", "temp": "45"}                                                                                                                                | {result": "success"}                                                        |
| GET     | /food/:key    | none                                                                                                                                                             | {"result": "success"}                                                       |
| POST    | /outfit/      | {"curr_city": "Chicago", "search_city": "Miami", "temp": "45", description: "light drizzle"}                                                                     | {"result": "success", "shoes": "boots", "top": "jacket", "bottom": "pants"} |
