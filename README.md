# Weather Or Not

# Install Packages

First, you must run the command "pip install --user yelpapi" to install the Yelp API package to the student machine.

# Customer Use / Client Use

First, run the command "python3.6 server/main.py" to run the server. Then, in a new terminal window, run the command "python3.6 app/app.py". Copy and paste the URL that will appear into your web browser, and this will start the Flask webapp.
To use the app, either create a city or type in the city you would like to search. The app will pull up weather data on the city.

To use Weather or Not, you just type in the city you would like to search.

# API Features

  - POST - This request will create a new city to add to the database, our return an outfit recommendation based on city info.
  - GET - This request will return weather info for a specified city, or return food info on a specified city.
  - PUT - This request will update weather info in a specified city.

# How to Run Test Script

To run the test script, you must first run the server by executing the command "python3.6 server/main.py". This will start a cherrypy server. Next, in a different terminal window, you should execute the command "python3.6 tests/test_ws.py", and this will test our API using the GET, POST, and PUT methods for the /weather/ resource and the /outfit/ resource.

# How to Run Server

Our server runs on port number 52097.

To run the server, type the command "python3.6 server/main.py". This will start our CherryPy server. Next, in a different terminal window, type the command "python3 tests/test_ws.py". This will run our test script to check the RESTful API functions of our server.

# How to Run Client

To run our client, start the server using the instructions above. Next, in a different terminal window run the command "python3.6 app/app.py", and in your web browser type in the URL "tinyurl.com/weather-paradigms". This will take you to the WeatherOrNot webapp, and you may begin making your search queries or creating cities.

# Functionality & Testing of Client

The webapp should allow you to search any city, and subsequently pull up the weather data in that corresponding city. The page will also display an outfit recommendation for that city, as well as a top coffee or ice cream recommendation for that city (depending on temperature in city, will choose ice cream or coffee.)
The outfit recommendation uses our own custom icons, and thresholds for what people should wear in certain temperatures. The food recommendation uses the Yelp API to return a top restaurant in that city.
The weather info is all from OpenWeatherMap's API.
By clicking the search a new city button, you can return to the home screen. Typing in a city that doesn't exist (i.e. yuiop) will take you to an error page.
From the home screen, you can also create a new city. This will let you fill out the weather data for that city, and then once the city is created you can then search that city from the normal home screen and its info will appear.
If you want to make edits to a city, you can click on whatever field you want to edit, change the value, and then hit "make edits", which will submit the edit to our database. Once you search this city again, the update will be reflected.


# Restful API Description

| Resource      | GET                                          | PUT                                      | POST                                            | DELETE |
|---------------|----------------------------------------------|------------------------------------------|-------------------------------------------------|--------|
| /weather/:key | Returns weather info for the city specified. |                                          |                                                 |        |
| /weather      |                                              | Updates weather info in a specified city | Creates a new city to be added to the database. |        |
| /outfit/      |                                              |                                          | Returns an outfit recommendation based on data. |        |
| /food/:key    | Returns food recommendations based on city.  |                                          |                                                 |        |

# JSON Guide

| Command | Resource      | Input Example                                                                                                                                                    | Output Example                                                              |
|---------|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| GET     | /weather/:key | none                                                                                                                                                             | {"result": "success", "data": "[cityinfo]"}                                 |
| POST    | /weather/     | {"timestamp": timestamp, "weather": [{"main": "Rain", "description": "light drizzle"}],"main":{"temp": "45"}, "wind":{"speed": "4"},"name": "Chicago","cod":200} | {"result": "success"}                                                       |
| PUT     | /weather/     | {"city": "Chicago", "temp": "45"}                                                                                                                                | {result": "success"}                                                        |
| GET     | /food/:key    | none                                                                                                                                                             | {"result": "success"}                                                       |
| POST    | /outfit/      | {"curr_city": "Chicago", "search_city": "Miami", "temp": "45", description: "light drizzle"}                                                                     | {"result": "success", "shoes": "boots", "top": "jacket", "bottom": "pants"} |
