import unittest
import requests
import json

class TestWeather(unittest.TestCase):

	SITE_URL = 'http://student04.cse.nd.edu:52097'
	WEATHER_URL = SITE_URL + '/weather/'
	OUTFIT_URL = SITE_URL + '/outfit/'

	def is_json(self, resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_weather_post(self):

		r = requests.post(self.WEATHER_URL, data=json.dumps({'city': 'Baileytown', 'description': 'bad city', 'temp': '12', 'wind': '12'}))

		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

	def test_weather_get(self):
        # possible reset funtion here: self.reset_data()
		p = requests.get(self.WEATHER_URL + 'Chicago')
		self.assertTrue(self.is_json(p.content.decode()))
		resp = json.loads(p.content.decode())
		self.assertEqual(resp['result'], 'success')

	def test_weather_put(self):

		p = requests.get(self.WEATHER_URL + 'Chicago')
		r = requests.put(self.WEATHER_URL, data=json.dumps({'city': 'Chicago', 'description': 'actually im from a suburb', 'temp': '12', 'wind': '12'}))
		p = requests.get(self.WEATHER_URL + 'Chicago')
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(p.content.decode())
		self.assertEqual(resp['data']['weather'][0]['description'], 'actually im from a suburb')
		
	def test_outfit_post(self):
		data = {'curr_city': 'Chicago', 'search_city': 'Miami', 'temp': '45 F', 'description': 'light drizzle'}
		r = requests.post(self.OUTFIT_URL, data=json.dumps(data))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['shoes'], 'boots')
		self.assertEqual(resp['top'], 'jacket')
		self.assertEqual(resp['bottom'], 'pants')

if __name__ == "__main__":
    unittest.main()
