import cherrypy
import json
import requests
import datetime

class WeatherController(object):

	def __init__(self):
		self.weather_dict = {}

	def GET(self, key):

		ret = { 'result': 'success' }

		# check if we do already have data for that city in our database
		if key not in self.weather_dict or 'refresh' in cherrypy.request.headers:
			# if not, we must first load that weather data into our database
			# requests.post("http://student04.cse.nd.edu:52097/weather/", data=json.dumps({'city': key}))
			api_key = 'e6f09841f68e6056d8149b8853ec7eb4'
			url_base = 'http://api.openweathermap.org/data/2.5/weather?q='
			url = url_base + str(key) + '&units=imperial&APPID=' + api_key

			r = requests.get(url).json()

			if r['cod'] != 200:
				ret['result'] = 'error'

			r['timestamp'] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
			self.weather_dict[key] = r

		ret['data'] = self.weather_dict[key]

		return json.dumps(ret)

	def POST(self):

		ret = { 'result': 'success' }

		data = json.loads(cherrypy.request.body.read())
		print(data)
		timestamp = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
		new_city = {"timestamp": timestamp, "weather": [{"main": "Rain", "description":data['description']}],"main":{"temp":data['temp']}, "wind":{"speed":data['wind']},"name":data['city'],"cod":200}

		self.weather_dict[data['city']] = new_city

		return json.dumps(ret)


	def PUT(self):

		print("put is running")
		ret = { 'result': 'success' }
		data = json.loads(cherrypy.request.body.read())

		# Structure of JSON values that we want to edit
		## "weather": [{..., "main": "Drizzle", "decription": "light intensity", ...}]

		# key entered by user should equal 'main' or 'description'
		try:
			self.weather_dict[data['city']]['weather'][0]['description'] = data['description']
			self.weather_dict[data['city']]['main']['temp'] = data['temp']
			self.weather_dict[data['city']]['wind']['speed'] = data['wind']
		except KeyError as ex:
			ret['result'] = 'error'
			ret['message'] = str(ex)
		except Exception as ex:
			ret['result'] = 'error'
			ret['message'] = str(ex)

		return json.dumps(ret)
