import cherrypy
import json
import requests
import datetime

class OutfitController(object):

    def __init__(self):
        self.outfit_dict = {}

    def POST(self):
        ret = { 'result': 'success' }
        data = json.loads(cherrypy.request.body.read())
        curr_city = data["curr_city"]
        search_city = data["search_city"]
        temp = float(data["temp"][:-2])
        description = data["description"]

        rainyWords = ["rain", "drizzle", "snow"]
        precipitation = False

        for word in rainyWords:
            if word in description.lower():
                precipitation = True
                break

        # get the outfit based on input and return it as JSON
        hat = False
        top = "tshirt"
        bottom = "shorts"
        shoes = "sneakers"

        # top recommender
        if (temp <= 40 or precipitation):
            top = "jacket"
        elif (40 < temp <= 55):
            top = "sweater"

        # bottom recommender
        if (temp <= 55):
            bottom = "pants"

        # shoes recommender
        if (precipitation):
            shoes = "boots"

        ret["top"] = top
        ret["bottom"] = bottom
        ret["shoes"] = shoes

        return json.dumps(ret)
