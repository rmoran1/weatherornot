import cherrypy
from _weather_controller import WeatherController
from _outfit_controller import OutfitController
from _food_controller import FoodController

def start_service():
	# object of controller
	wc = WeatherController()
	oc = OutfitController()
	fc = FoodController()

	# create dispatcher
	dispatcher = cherrypy.dispatch.RoutesDispatcher()
	dispatcher.connect('weather_get', '/weather/:key', controller=wc, action = 'GET', conditions=dict(method=['GET']))
	dispatcher.connect('weather_post', '/weather/', controller=wc, action = 'POST', conditions=dict(method=['POST']))
	dispatcher.connect('weather_put', '/weather/', controller=wc, action = 'PUT', conditions=dict(method=['PUT']))

	dispatcher.connect('outfit_post', '/outfit/', controller=oc, action = 'POST', conditions=dict(method=['POST']))

	dispatcher.connect('food_get', '/food/:city', controller=fc, action = 'GET', conditions=dict(method=['GET']))
	
	#configuration for server
	conf = {
			'global' : {
				'server.socket_host' : 'student04.cse.nd.edu',
				'server.socket_port' : 52097,
				},
			'/' : { 'request.dispatch' : dispatcher },
			}

	#update config
	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

if __name__ == '__main__':
	start_service()
