import cherrypy
import json
import requests
from yelpapi import YelpAPI

class FoodController(object):

    def __init__(self):
        self.food_dict = {}

    def GET(self, city):
        ret = { 'result': 'success' }

        MY_API_KEY = "XcexhTbsXafG0FoVJrZV1ji6unnD4VzMs_6U8deo7UpCPSmK_ohq11RVvEnscBy1slh3eIW63F1IPIvhyQ6zgI9AucWxaZmbbN1OwPxXSOr8SGrtGfik90yhyrj9W3Yx"
        yelp_api =  YelpAPI(MY_API_KEY, timeout_s=3.0)

        ice_cream_results = yelp_api.search_query(location= city, term= "ice cream", sort_by= "rating", limit= 5)
        coffee_results = yelp_api.search_query(location= city, term= "coffee", sort_by= "rating", limit= 5)

        # final return dictionary
        food_dict = {"cold_restaurants": coffee_results, "warm_restaurants": ice_cream_results}

        return json.dumps(food_dict)
